from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import RegexpTokenizer
import collections, re
import numpy as np
from tqdm import tqdm
import datetime

lmtzr = WordNetLemmatizer()    
stop = stopwords.words('english')
reg_tokenizer = RegexpTokenizer('\w\w+')

#Replace numbers with service token
NUM_RE = re.compile('^[+-]?[0-9]+\.?[0-9]*$')
# def replace_numbers(tokenized_texts):
    # return [[token if not NUM_RE.match(token) else '<num>' for token in text] for text in tokenized_texts]

def replace_numbers(tokenized_texts):
    return [token if not NUM_RE.match(token) else '<num>' for token in tokenized_texts]

def preprocess_train(in_path, out_path):#(domain, in_path, out_path):
    f = open(in_path, 'r')
    out = open(out_path, 'w')
    
    text = f.readlines()#[:1000]
    f.close()
    
    sents_lemmatized = []
    sents_nonumbers = []
    one_sent_txt = []
    one_txt_nonumbers = []
    for line in tqdm(text, desc="text preprocessing"):
        txt_token = reg_tokenizer.tokenize(line.lower())
        if len(txt_token)==0:
            continue
        txt_stop = [i for i in txt_token if i not in stop]
        txt_lemm = [lmtzr.lemmatize(w) for w in txt_stop]
        txt_num = replace_numbers(txt_lemm)
        sents_lemmatized.append(txt_lemm)
        sents_nonumbers.append(txt_num)
        one_sent_txt.extend(txt_lemm)
        one_txt_nonumbers.extend(txt_num)
        print(*txt_lemm, sep=' ', end='\n', file=out)
#         out.write(' '.join(txt_lemm)+'\n')

    
    return one_sent_txt, one_txt_nonumbers, sents_lemmatized, sents_nonumbers
#     return one_sent_txt, sents_lemmatized


def create_vocab(vocab_file_path, tokenized_texts, max_size=100000):
    word_counts = collections.defaultdict(int)
    
    total_words = 0
    docs = 0
    
#     print("Creating vocab")
    
    for txt in tqdm(tokenized_texts, desc="Creating vocab"):
        docs += 1
        for word in set(txt):
            word_counts[word] += 1
            total_words += 1
    
    
    sorted_word_counts = sorted(word_counts.items(), reverse=True, key=lambda pair: pair[1])
    sorted_word_counts = [('<pad>', 0), ('<unk>', 0)] + sorted_word_counts

    if len(word_counts) > max_size:
        sorted_word_counts = sorted_word_counts[:max_size]
        
    word2id = {word: i for i, (word, _) in enumerate(sorted_word_counts)}
    id2word = {i: word for i, (word, _) in enumerate(sorted_word_counts)}
    
    word2freq = np.array([cnt / docs for _, cnt in sorted_word_counts])
    
#     print()
    
    vocab_file = open(vocab_file_path, mode='w', encoding='utf8')
    for word, _ in tqdm(sorted_word_counts, desc="to file"):
        vocab_file.write(word +'\n')
    vocab_file.close()

    return word2id, id2word, sorted_word_counts, word2freq

def texts_to_ids(tokenized_texts, word2id):
    return [[word2id.get(token, 1) for token in text] for text in tokenized_texts]

def preprocess_test(in_path, labels_path, out_path, out_labels_path):
    # For restaurant domain, only keep sentences with single 
    # aspect label that in {Food, Staff, Ambience}

    f1 = open(in_path, 'r')
    f2 = open(labels_path, 'r')
    out1 = open(out_path, 'w')
    out2 = open(out_labels_path, 'w')
    
    texts = f1.readlines()
    labels = f2.readlines()
    f1.close()
    f2.close()

    sents_lemmatized = []
    sents_nonumbers = []
    one_sent_txt = []
    one_txt_nonumbers = []

    for txt, label in tqdm(zip(texts, labels), desc="preprocessing test"):
        label = label.strip()
        if label not in ['Food', 'Staff', 'Ambience']:
            continue
        
        txt_token = reg_tokenizer.tokenize(txt.lower())
        if len(txt_token)==0:
            continue
        txt_stop = [i for i in txt_token if i not in stop]
        txt_lemm = [lmtzr.lemmatize(w) for w in txt_stop]
        txt_num = replace_numbers(txt_lemm)
        sents_lemmatized.append(txt_lemm)
        sents_nonumbers.append(txt_num)
        # one_sent_txt = sum(sents_lemmatized, [])
        one_sent_txt.extend(txt_lemm)
        # one_txt_nonumbers = sum(sents_nonumbers, [])
        one_txt_nonumbers.extend(txt_num)
#         out1.write(' '.join(txt_lemm)+'\n')
        
        if len(txt_lemm) > 0:
            print(*txt_lemm, sep=' ', end='\n', file=out1)
            print(label, end='\n', file=out2)
#             out1.write(' '.join(txt_lemm) + '\n')
#             out2.write(label+'\n')

           
    return one_sent_txt, one_txt_nonumbers, sents_lemmatized, sents_nonumbers